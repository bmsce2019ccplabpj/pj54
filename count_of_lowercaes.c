#include<stdio.h>
int main()
{
    char str[20];
    printf("Enter the string:\n");
    gets(str);
    int lower=0,upper=0,digits=0,i=0;
    while(str[i]!='\0')
    {
        if(str[i]>='a'&&str[i]<='z')
        {
             lower++;
        }
        else if(str[i]>='A'&&str[i]<='Z')
        {
             upper++;
        }
        else if(str[i]>='0'&&str[i]<='9')
        {
             digits++;
        }
        else if(str[i]=='*')
        {
            break;
        }
        i++;
    }
    printf("The number of upper case characters in the string = %d\n",upper);
    printf("The number of lower case characters in the string = %d\n",lower);
    printf("The number of digits in the string = %d\n",digits);
    return 0;
}
